library(corrplot)
library(ggplot2)
library(dplyr)
library(caTools)
library(rpart)
library(rpart.plot)
library(pROC)
library(tm)
library(ROSE)
library(randomForest)
library(lubridate)

#1
#a
carln_data.raw <- read.csv('carInsurance_train.csv')
carln<-carln_data.raw
str(carln)
summary(carln)
carln$CarInsurance<-factor(carln$CarInsurance,levels = 0:1,labels = c('no','yes'))
ggplot(carln, aes(CarInsurance)) + geom_bar()#Relatively balanced

#b
table(is.na(carln$CallEnd))
table(is.na(carln$CallStart))


Sys.setlocale("LC_TIME", "English")


carln$duration<- difftime(strptime(carln$CallEnd,format = "%H:%M:%S"),
                       strptime(carln$CallStart,format = "%H:%M:%S"),units="mins")                      

#c
summary(carln)
car.df <- carln[,c("duration", "CarInsurance", "Job","Age","CarLoan","Communication","NoOfContacts","HHInsurance","Education","Marital","Balance")]

#2
str(car.df)
car.df$CarLoan<-factor(car.df$CarLoan,levels = 0:1,labels = c('no','yes'))
ggplot(car.df, aes(Communication)) + geom_bar()#alot na
ggplot(car.df) +aes(x = Communication ,fill = CarInsurance) + geom_bar(position = "fill")#not
car.df$Communication<-NULL
ggplot(car.df, aes(duration)) + geom_histogram()
d<-table(car.df$duration>45)
corrces<-function(x,by){
  return(ifelse(x<by,x,by))
  
}
car.df$duration<-sapply(car.df$duration,by=45,corrces)
ggplot(car.df, aes(Age)) + geom_histogram()
table(car.df$Age)
car.df$Age<-sapply(car.df$Age,by=77,corrces)
summary(car.df)
ggplot(car.df, aes(Job)) + geom_bar()#I will add the na values to the common value
table(car.df$Job)
car.df$Job<-as.character(car.df$Job)
table(car.df$Job)<-as.factor(car.df$Job)
make_na_job<-function(x){
  return(ifelse(is.na(x),"management" ,x))
}
car.df$Job<-sapply(car.df$Job,make_na_job)
car.df$Job<-as.factor(car.df$Job)
str(car.df)
ggplot(car.df, aes(NoOfContacts)) + geom_histogram()
table(car.df$NoOfContacts)
ggplot(car.df, aes(Education)) + geom_bar()
table(car.df$NoOfContacts)
make_na_ed<-function(x){
  return(ifelse(is.na(x),"secondary" ,x))
}

car.df$Education<-sapply(car.df$Education,make_na_ed)
car.df$Education<-as.factor(car.df$Education)
ggplot(car.df, aes(Balance)) + geom_histogram()+xlim(0,50000)
table(car.df$Balance)
summary(car.df)
str(car.df)

filter <- sample.split(car.df$CarInsurance, SplitRatio = 0.7)
car.train <- subset(car.df, filter == TRUE)
car.test <- subset(car.df, filter == FALSE)

dim(car.df)
dim(car.train)
dim(car.test)
#3

#A
model.dt<-rpart(CarInsurance~.,data=car.train)
rpart.plot(model.dt, box.palette = "RdBu", shadow.col = "grey", nn = TRUE)
prp(model.dt)



prediction.dt<- predict(model.dt,car.test,type = 'prob')[,"yes"]
actual.dt <- car.test$CarInsurance
#c

model.rf <- randomForest(CarInsurance~ .,data=car.train, importance = TRUE)

prediction.rf <- predict(model.rf,car.test,type = 'prob')[,"yes"]
actual.rf <- car.test$CarInsurance

cf.rf <- table(actual.rf,prediction.rf > 0.6)
precision <- cf.rf['yes','TRUE']/(cf.rf['yes','TRUE'] + cf.rf['no','TRUE'])
recall <- cf.rf['yes','TRUE']/(cf.rf['yes','TRUE'] + cf.rf['yes','FALSE'])
precision
recall
#ROC curve 




rocCurveDt <- roc(actual.dt,prediction.dt, direction = "<", levels = c("no", "yes"))
rocCurveRF <- roc(actual.rf,prediction.rf, direction = "<", levels = c("no", "yes"))

#plot the chart 
plot(rocCurveDt, col = 'red',main = "ROC Chart")
par(new = TRUE)
plot(rocCurveRF, col = 'blue',main = "ROC Chart")

auc(rocCurveDt)
auc(rocCurveRF)

#4
costcall.avr<-100
join<-200
d<-table(car.df$CarInsurance)

totalcoast.avr.before<-(d[2]*200)-(d[1]*100)


pre<-(cf.rf[1,2]+cf.rf[2,2])/(cf.rf[1,2]+cf.rf[2,2]+cf.rf[1,1]+cf.rf[2,1])
pre




